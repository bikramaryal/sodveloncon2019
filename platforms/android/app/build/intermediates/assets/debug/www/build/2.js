webpackJsonp([2],{

/***/ 760:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SpeakerListPageModule", function() { return SpeakerListPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__speaker_list__ = __webpack_require__(770);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SpeakerListPageModule = /** @class */ (function () {
    function SpeakerListPageModule() {
    }
    SpeakerListPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_0__speaker_list__["a" /* SpeakerListPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_0__speaker_list__["a" /* SpeakerListPage */]),
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_0__speaker_list__["a" /* SpeakerListPage */]
            ]
        })
    ], SpeakerListPageModule);
    return SpeakerListPageModule;
}());

//# sourceMappingURL=speaker-list.module.js.map

/***/ }),

/***/ 770:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SpeakerListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_in_app_browser__ = __webpack_require__(377);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_conference_data__ = __webpack_require__(165);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SpeakerListPage = /** @class */ (function () {
    function SpeakerListPage(actionSheetCtrl, navCtrl, confData, config, inAppBrowser) {
        this.actionSheetCtrl = actionSheetCtrl;
        this.navCtrl = navCtrl;
        this.confData = confData;
        this.config = config;
        this.inAppBrowser = inAppBrowser;
        this.speakers = [];
    }
    SpeakerListPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.confData.getSpeakers().subscribe(function (speakers) {
            _this.speakers = speakers;
        });
    };
    SpeakerListPage.prototype.goToSessionDetail = function (session) {
        this.navCtrl.push("SessionDetailPage", session);
    };
    SpeakerListPage.prototype.goToSpeakerDetail = function (speakerName) {
        this.navCtrl.push("SpeakerDetailPage", speakerName);
    };
    SpeakerListPage.prototype.goToSpeakerTwitter = function (speaker) {
        this.inAppBrowser.create("https://twitter.com/" + speaker.twitter, '_blank');
    };
    SpeakerListPage.prototype.openSpeakerShare = function (speaker) {
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Share ' + speaker.name,
        });
        actionSheet.present();
    };
    SpeakerListPage.prototype.openContact = function (speaker) {
        var mode = this.config.get('mode');
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Contact ' + speaker.name,
            buttons: [
                {
                    text: "Email ( " + speaker.email + " )",
                    icon: mode !== 'ios' ? 'mail' : null,
                    handler: function () {
                        window.open('mailto:' + speaker.email);
                    }
                },
                {
                    text: "Call ( " + speaker.phone + " )",
                    icon: mode !== 'ios' ? 'call' : null,
                    handler: function () {
                        window.open('tel:' + speaker.phone);
                    }
                }
            ]
        });
        actionSheet.present();
    };
    SpeakerListPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-speaker-list',template:/*ion-inline-start:"C:\Users\Bikram\Desktop\TEMP\sodveloncon2019\src\pages\speaker-list\speaker-list.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>Speakers</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content class="outer-content speaker-list">\n\n  <ion-list>\n\n    <ion-grid fixed>\n\n      <ion-row align-items-stretch>\n\n        <ion-col col-12 col-md-6 align-self-stretch align-self-center *ngFor="let speaker of speakers">\n\n          <ion-card class="speaker-card">\n\n\n\n            <ion-card-header>\n\n              <button ion-item detail-none (click)="goToSpeakerDetail(speaker)">\n\n        <ion-avatar item-left>\n\n          <img [src]="speaker.profilePic" alt="Speaker profile pic">\n\n        </ion-avatar>\n\n        {{speaker.name}}\n\n      </button>\n\n            </ion-card-header>\n\n\n\n            <ion-card-content class="outer-content">\n\n              <ion-list>\n\n                <button ion-item *ngFor="let session of speaker.sessions" (click)="goToSessionDetail(session)">\n\n          <h3>{{session.name}}</h3>\n\n        </button>\n\n\n\n                <button ion-item (click)="goToSpeakerDetail(speaker)">\n\n          <h3>About {{speaker.name}}</h3>\n\n        </button>\n\n              </ion-list>\n\n            </ion-card-content>\n\n\n\n            <ion-row no-padding>\n\n              <ion-col>\n\n                <!-- <button ion-button clear small color="primary" icon-left (click)="goToSpeakerTwitter(speaker)">\n\n          <ion-icon name="logo-twitter"></ion-icon>\n\n          Tweet\n\n        </button> -->\n\n              </ion-col>\n\n              <ion-col text-center>\n\n                <!-- <button ion-button clear small color="primary" icon-left (click)="openSpeakerShare(speaker)">\n\n          <ion-icon name=\'share-alt\'></ion-icon>\n\n         Share\n\n        </button> -->\n\n              </ion-col>\n\n              <ion-col text-right>\n\n                <!-- <button ion-button clear small color="primary" icon-left (click)="openContact(speaker)">\n\n          <ion-icon name=\'chatboxes\'></ion-icon>\n\n          Contact\n\n        </button> -->\n\n              </ion-col>\n\n            </ion-row>\n\n\n\n          </ion-card>\n\n\n\n\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-grid>\n\n  </ion-list>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\Bikram\Desktop\TEMP\sodveloncon2019\src\pages\speaker-list\speaker-list.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_conference_data__["a" /* ConferenceData */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Config */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_in_app_browser__["a" /* InAppBrowser */]])
    ], SpeakerListPage);
    return SpeakerListPage;
}());

//# sourceMappingURL=speaker-list.js.map

/***/ })

});
//# sourceMappingURL=2.js.map