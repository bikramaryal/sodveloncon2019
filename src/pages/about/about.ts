import { Component } from '@angular/core';

import { PopoverController, IonicPage } from 'ionic-angular';

// import { PopoverPage } from '../about-popover/about-popover';

@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
  conferenceDate = '2019-04-07';

  constructor() { }

  // presentPopover(event: Event) {
  //   let popover = this.popoverCtrl.create(PopoverPage);
  //   popover.present({ ev: event });
  // }
}
