webpackJsonp([11],{

/***/ 162:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SupportPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(121);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_angularfire2_database__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_user_data__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(51);
//   selector: 'page-user',
//   templateUrl: 'support.html'
// })
// export class SupportPage {
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
//   submitted: boolean = false;
//   supportMessage: string;
//   constructor(
//     public navCtrl: NavController,
//     public alertCtrl: AlertController,
//     public toastCtrl: ToastController
//   ) {
//   }
//   ionViewDidEnter() {
//     let toast = this.toastCtrl.create({
//       message: 'This does not actually send a support request.',
//       duration: 3000
//     });
//     toast.present();
//   }
//   // If the user enters text in the support question and then navigates
//   // without submitting first, ask if they meant to leave the page
//   ionViewCanLeave(): boolean | Promise<boolean> {
//     // If the support message is empty we should just navigate
//     if (!this.supportMessage || this.supportMessage.trim().length === 0) {
//       return true;
//     }
//     return new Promise((resolve: any, reject: any) => {
//       let alert = this.alertCtrl.create({
//         title: 'Leave this page?',
//         message: 'Are you sure you want to leave this page? Your support message will not be submitted.'
//       });
//       alert.addButton({ text: 'Stay', handler: reject });
//       alert.addButton({ text: 'Leave', role: 'cancel', handler: resolve });
//       alert.present();
//     });
//   }
// }





var SupportPage = /** @class */ (function () {
    function SupportPage(navParam, db, toastCtrl, navCtrl, userData, storage) {
        this.navParam = navParam;
        this.db = db;
        this.toastCtrl = toastCtrl;
        this.navCtrl = navCtrl;
        this.userData = userData;
        this.storage = storage;
        this.message = '';
        this.submitted = false;
        // console.log(this.storage.get('username'))
        // this.userData.getUsername().then((username) => {
        //   this.username = username;
        // }); 
        // this.getUsername();
        //console.log(this.username)
        this._chatSubscription = this.db.object('/chat').valueChanges().subscribe(function (data) {
            console.log(data);
        });
    }
    SupportPage.prototype.sendMessage = function () {
        var _this = this;
        this.userData.getUsername().then(function (username) {
            _this.username = username;
            console.log(_this.username);
            if (_this.message.length > 15) {
                _this.db.list('/chat').push({
                    username: _this.username,
                    message: _this.message
                });
            }
        }).then(function () {
            if (_this.message.length > 15) {
                _this.toastOptions = {
                    message: "Your support message has been sent successfully",
                    duration: 3000
                };
                _this.toastCtrl.create(_this.toastOptions).present();
            }
            else {
                _this.toastOptions = {
                    message: "Unable to send support Request!",
                    duration: 3000
                };
                _this.toastCtrl.create(_this.toastOptions).present();
            }
            //message is sent
            _this.message = "";
            _this.submitted = true;
        }).catch(function () {
            //some error maybe firebase unreachable
        });
    };
    // sendMessage(){ this.db.list('/chat').push({
    //   username: this.username,
    //   message: this.message
    // })
    SupportPage.prototype.ionViewDidLoad = function () {
        // this.db.list('/chat').push({
        //   specialMessage: true,
        //   message: `${this.username} has joined the room`
        // });
    };
    SupportPage.prototype.ionViewWillLeave = function () {
        // this._chatSubscription.unsubscribe();
        // this.db.list('/chat').push({
        //   specialMessage: true,
        //   message: `${this.username} has left the room`
        // });
    };
    SupportPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-support',template:/*ion-inline-start:"C:\Users\Bikram\Desktop\TEMP\sodveloncon2019\src\pages\support\support.html"*/'<ion-header>\n\n\n\n	<ion-navbar>\n\n		<button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n		<ion-title>Support</ion-title>\n\n	</ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n\n\n\n\n<ion-content>\n\n	<h3>Enter your Queries Here:</h3>\n\n	\n\n		<ion-toolbar>\n\n		  <div padding>\n\n			<div class="elem"><ion-input type="text" placeholder="Type here..." [(ngModel)]="message"></ion-input></div>\n\n			<!-- <div class="elem"><button ion-button icon-only (click)="sendMessage()"><ion-icon name="send"></ion-icon> </button></div> -->\n\n			</div>\n\n\n\n			\n\n			\n\n			\n\n		</ion-toolbar>\n\n		<p>For more information and support contact us at http://sodveloncon2019.com/</p>\n\n		<button ion-button round (click)="sendMessage()">Send</button>\n\n\n\n	  </ion-content>\n\n'/*ion-inline-end:"C:\Users\Bikram\Desktop\TEMP\sodveloncon2019\src\pages\support\support.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["AngularFireDatabase"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__providers_user_data__["a" /* UserData */], __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */]])
    ], SupportPage);
    return SupportPage;
}());

//# sourceMappingURL=support.js.map

/***/ }),

/***/ 165:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConferenceData; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(327);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__user_data__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(268);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_observable_of__ = __webpack_require__(232);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_observable_of___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_observable_of__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ConferenceData = /** @class */ (function () {
    //data2: any;
    function ConferenceData(http, user) {
        this.http = http;
        this.user = user;
    }
    ConferenceData.prototype.load = function () {
        if (this.data) {
            return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].of(this.data);
        }
        else {
            return this.http.get('assets/data/data.json')
                .map(this.processData, this);
        }
    };
    // load2(): any {
    //   if (this.data2) {
    //     return Observable.of(this.data2);
    //   } else {
    //     return this.http.get('assets/data/data.json')
    //       .map(this.processData2, this);
    //   }
    // }
    ConferenceData.prototype.processData = function (data) {
        var _this = this;
        // just some good 'ol JS fun with objects and arrays
        // build up the data by linking speakers to sessions
        this.data = data.json();
        this.data.tracks = [];
        // loop through each day in the schedule
        this.data.schedule.forEach(function (day) {
            // loop through each timeline group in the day
            day.groups.forEach(function (group) {
                // loop through each session in the timeline group
                group.sessions.forEach(function (session) {
                    session.speakers = [];
                    if (session.speakerNames) {
                        session.speakerNames.forEach(function (speakerName) {
                            var speaker = _this.data.speakers.find(function (s) { return s.name === speakerName; });
                            if (speaker) {
                                session.speakers.push(speaker);
                                speaker.sessions = speaker.sessions || [];
                                speaker.sessions.push(session);
                            }
                        });
                    }
                    if (session.tracks) {
                        session.tracks.forEach(function (track) {
                            if (_this.data.tracks.indexOf(track) < 0) {
                                _this.data.tracks.push(track);
                            }
                        });
                    }
                });
            });
        });
        return this.data;
    };
    // processData2(data2: any) {
    //   // just some good 'ol JS fun with objects and arrays
    //   // build up the data2 by linking speakers to sessions
    //   this.data2 = data2.json();
    //   this.data2.tracks = [];
    //   // loop through each day in the schedule
    //   this.data2.schedule2.forEach((day: any) => {
    //     // loop through each timeline group in the day
    //     day.groups.forEach((group: any) => {
    //       // loop through each session in the timeline group
    //       group.sessions.forEach((session: any) => {
    //         session.speakers = [];
    //         if (session.speakerNames) {
    //           session.speakerNames.forEach((speakerName: any) => {
    //             let speaker = this.data2.speakers.find((s: any) => s.name === speakerName);
    //             if (speaker) {
    //               session.speakers.push(speaker);
    //               speaker.sessions = speaker.sessions || [];
    //               speaker.sessions.push(session);
    //             }
    //           });
    //         }
    //         if (session.tracks) {
    //           session.tracks.forEach((track: any) => {
    //             if (this.data2.tracks.indexOf(track) < 0) {
    //               this.data2.tracks.push(track);
    //             }
    //           });
    //         }
    //       });
    //     });
    //   });
    //   return this.data2;
    // }
    ConferenceData.prototype.getTimeline = function (dayIndex, queryText, excludeTracks, segment) {
        var _this = this;
        if (queryText === void 0) { queryText = ''; }
        if (excludeTracks === void 0) { excludeTracks = []; }
        if (segment === void 0) { segment = 'day1'; }
        return this.load().map(function (data) {
            var day = data.schedule[dayIndex];
            day.shownSessions = 0;
            queryText = queryText.toLowerCase().replace(/,|\.|-/g, ' ');
            var queryWords = queryText.split(' ').filter(function (w) { return !!w.trim().length; });
            day.groups.forEach(function (group) {
                group.hide = true;
                group.sessions.forEach(function (session) {
                    // check if this session should show or not
                    _this.filterSession(session, queryWords, excludeTracks, segment);
                    if (!session.hide) {
                        // if this session is not hidden then this group should show
                        group.hide = false;
                        day.shownSessions++;
                    }
                });
            });
            return day;
        });
    };
    // getTimeline2(dayIndex: number, queryText = '', excludeTracks: any[] = [], segment = 'day2') {
    //   return this.load2().map((data2: any) => {
    //     let day = data2.schedule2[dayIndex];
    //     day.shownSessions = 0;
    //     queryText = queryText.toLowerCase().replace(/,|\.|-/g, ' ');
    //     let queryWords = queryText.split(' ').filter(w => !!w.trim().length);
    //     day.groups.forEach((group: any) => {
    //       group.hide = true;
    //       group.sessions.forEach((session: any) => {
    //         // check if this session should show or not
    //         this.filterSession(session, queryWords, excludeTracks, segment);
    //         if (!session.hide) {
    //           // if this session is not hidden then this group should show
    //           group.hide = false;
    //           day.shownSessions++;
    //         }
    //       });
    //     });
    //     return day;
    //   });
    // }
    ConferenceData.prototype.filterSession = function (session, queryWords, excludeTracks, segment) {
        var matchesQueryText = false;
        if (queryWords.length) {
            // of any query word is in the session name than it passes the query test
            queryWords.forEach(function (queryWord) {
                if (session.name.toLowerCase().indexOf(queryWord) > -1) {
                    matchesQueryText = true;
                }
            });
        }
        else {
            // if there are no query words then this session passes the query test
            matchesQueryText = true;
        }
        // if any of the sessions tracks are not in the
        // exclude tracks then this session passes the track test
        var matchesTracks = false;
        session.tracks.forEach(function (trackName) {
            if (excludeTracks.indexOf(trackName) === -1) {
                matchesTracks = true;
            }
        });
        // if the segement is 'favorites', but session is not a user favorite
        // then this session does not pass the segment test
        var matchesSegment = false;
        if (segment === 'favorites') {
            if (this.user.hasFavorite(session.name)) {
                matchesSegment = true;
            }
        }
        else {
            matchesSegment = true;
        }
        // all tests must be true if it should not be hidden
        session.hide = !(matchesQueryText && matchesTracks && matchesSegment);
    };
    ConferenceData.prototype.getSpeakers = function () {
        return this.load().map(function (data) {
            return data.speakers.sort(function (a, b) {
                var aName = a.name.split(' ').pop();
                var bName = b.name.split(' ').pop();
                return aName.localeCompare(bName);
            });
        });
    };
    ConferenceData.prototype.getTracks = function () {
        return this.load().map(function (data) {
            return data.tracks.sort();
        });
    };
    ConferenceData.prototype.getMap = function () {
        return this.load().map(function (data) {
            return data.map;
        });
    };
    ConferenceData = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_2__user_data__["a" /* UserData */]])
    ], ConferenceData);
    return ConferenceData;
}());

//# sourceMappingURL=conference-data.js.map

/***/ }),

/***/ 166:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DownloadPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ionic_storage__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_user_data__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(121);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_angularfire2_database__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ts_md5_dist_md5__ = __webpack_require__(700);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ts_md5_dist_md5___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_ts_md5_dist_md5__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};







/**
 * Generated class for the DownloadPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DownloadPage = /** @class */ (function () {
    function DownloadPage(navCtrl, toastCtrl, userData, storage, db, navParams, events1) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.toastCtrl = toastCtrl;
        this.userData = userData;
        this.storage = storage;
        this.db = db;
        this.navParams = navParams;
        this.events1 = events1;
        this.count = 1;
        this.rating = 4;
        this.eventDate = new Date('2019-04-08');
        this.electionDate = new Date('2019-04-07');
        this.rateStatus = 'notDone';
        this.today = Date.now();
        events1.subscribe('star-rating:changed', function (starRating) {
            console.log(starRating);
            _this.rating = starRating;
        });
    }
    DownloadPage.prototype.ngAfterViewInit = function () {
        this.getRateStatus();
    };
    DownloadPage.prototype.nextRating = function () {
        var _this = this;
        this.userData.getUsername().then(function (username) {
            _this.username = username;
            _this.hashUsername = username;
            _this.db.list('/rating').push({
                username: _this.username,
                question: _this.count,
                ratingValue: _this.rating
            }).then(function () {
                if (_this.count == 5) {
                    _this.Submit();
                    _this.userData.setRateStatus('done');
                }
                _this.count++;
            });
            console.log("Some error occured");
        });
    };
    // previousRating(){
    //   console.log("Previous Rating button clicked");
    //   this.count--;
    //   }
    DownloadPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log('ionViewDidLoad DownloadPage');
        this.userData.getUsername().then(function (username) {
            _this.hashString = String(__WEBPACK_IMPORTED_MODULE_5_ts_md5_dist_md5__["Md5"].hashStr(username));
        });
    };
    DownloadPage.prototype.Submit = function () {
        console.log("Submit Successful");
        this.toastOptions = {
            message: "Review has been recorded! Thank you!",
            duration: 3000
        };
        this.toastCtrl.create(this.toastOptions).present();
    };
    DownloadPage.prototype.getRateStatus = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.userData.checkHasRateApp().then(function (rateStatus) {
                    _this.rateStatus = rateStatus;
                });
                return [2 /*return*/];
            });
        });
    };
    DownloadPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["m" /* Component */])({
            selector: 'page-download',template:/*ion-inline-start:"C:\Users\Bikram\Desktop\TEMP\sodveloncon2019\src\pages\download\download.html"*/'<ion-header>\n\n  <ion-navbar>\n\n      <button ion-button menuToggle>\n\n          <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n    <ion-title>Downloads</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n<!-- && !(rateStatus == \'done\') -->\n\n<ion-content padding>\n\n    <h3 class="greenText"><a href="http://sodveloncon2019.com/downloads/abstract.pdf">DOWNLOAD ABSTRACT BOOK</a></h3>\n\n    <h3 class="greenText"><a href="http://sodveloncon2019.com/downloads/posters">DOWNLOAD POSTERS</a></h3>\n\n \n\n<div *ngIf="eventDate < today && !(rateStatus == \'done\')">\n\n    <div class="card-title">\n\n      <p style="font-weight: bold; font-size: 20px; color: rgb(84, 84, 141);" absolute-drag>Please rate the Mobile App according to:</p>\n\n    </div>\n\n      <p style="font-weight: bold; font-size: 15px;color: rgb(117, 185, 48)">{{count}} of 5</p>\n\n    <div *ngIf="count == 1">\n\n              <h3 style="font-weight: bold">{{count}}.Ease of use</h3>\n\n              <ionic3-star-rating \n\n                  activeIcon = "ios-star"\n\n                  defaultIcon = "ios-star-outline"\n\n                  activeColor = "#488aff" \n\n                  defaultColor = "red"\n\n                  readonly="false"\n\n                  [rating]="rating">\n\n            </ionic3-star-rating>\n\n            Selected rating: {{rating}}\n\n\n\n            <br>\n\n            <ion-buttons end>\n\n              <button ion-button (click)="nextRating()" type="submit" solt="end">Next</button>\n\n            </ion-buttons>\n\n    </div>\n\n\n\n<div *ngIf="count == 2">\n\n    <h3 style="font-weight: bold">{{count}}.Performance</h3>\n\n  <ionic3-star-rating \n\n      activeIcon = "ios-star"\n\n      defaultIcon = "ios-star-outline"\n\n      activeColor = "#488aff" \n\n      defaultColor = "red"\n\n      readonly="false"\n\n      [rating]="rating">\n\n</ionic3-star-rating>\n\nSelected rating: {{rating}}\n\n<br>\n\n  <!-- <button ion-button (click)="previousRating()" type="submit" slot="start">Prevous</button> -->\n\n  <ion-buttons end>\n\n      <button ion-button (click)="nextRating()" type="submit" solt="end">Next</button>\n\n    </ion-buttons>\n\n</div>\n\n\n\n<div *ngIf="count == 3">\n\n    <h3 style="font-weight: bold">{{count}}.Outlook</h3>\n\n  <ionic3-star-rating \n\n      activeIcon = "ios-star"\n\n      defaultIcon = "ios-star-outline"\n\n      activeColor = "#488aff" \n\n      defaultColor = "red"\n\n      readonly="false"\n\n      [rating]="rating">\n\n</ionic3-star-rating>\n\nSelected rating: {{rating}}\n\n<br>\n\n  <!-- <button ion-button (click)="previousRating()" type="submit" slot="start">Prevous</button> -->\n\n  <ion-buttons end>\n\n      <button ion-button (click)="nextRating()" type="submit" solt="end">Next</button>\n\n    </ion-buttons>\n\n</div>\n\n\n\n<div *ngIf="count == 4">\n\n    <h3 style="font-weight: bold">{{count}}.Preference compared to Papers</h3>\n\n  <ionic3-star-rating \n\n      activeIcon = "ios-star"\n\n      defaultIcon = "ios-star-outline"\n\n      activeColor = "#488aff" \n\n      defaultColor = "red"\n\n      readonly="false"\n\n      [rating]="rating">\n\n</ionic3-star-rating>\n\nSelected rating: {{rating}}\n\n<br>\n\n  <!-- <button ion-button (click)="previousRating()" type="submit" slot="start">Prevous</button> -->\n\n  <ion-buttons end>\n\n      <button ion-button (click)="nextRating()" type="submit" solt="end">Next</button>\n\n    </ion-buttons>\n\n</div>\n\n\n\n<div *ngIf="count == 5">\n\n        <h3 style="font-weight: bold">{{count}}.If you will recommend mobile app to next conference</h3>\n\n      <ionic3-star-rating \n\n          activeIcon = "ios-star"\n\n          defaultIcon = "ios-star-outline"\n\n          activeColor = "#488aff" \n\n          defaultColor = "red"\n\n          readonly="false"\n\n          [rating]="rating">\n\n    </ionic3-star-rating>\n\n    Selected rating: {{rating}}\n\n    <br>\n\n    <br>\n\n    <ion-buttons end>\n\n        <button ion-button (click)="nextRating()" type="submit" solt="end">Submit</button>\n\n      </ion-buttons>\n\n</div>\n\n<div *ngIf="count <= 5">  \n\n    <img src="assets/img/rating.png">  \n\n</div>\n\n\n\n</div>\n\n\n\n <div *ngIf="rateStatus == \'done\' || count > 5">\n\n   <br>\n\n   <br>\n\n  <h4 style="font-size: 15px;text-align: center;font-family: \'Segoe UI\', Tahoma, Geneva, Verdana, sans-serif">CONGRATULATIONS, now you\'re able download certificate here!</h4>\n\n  <p class="greenText"><a href="http://sodveloncon2019.com/downloads/{{hashString}}">DOWNLOAD CERTIFICATE</a></p>\n\n</div>\n\n\n\n<div *ngIf="electionDate < today">\n\n    <h3 class="greenText"><a href="http://sodveloncon2019.com/election">ELECTION PORTAL</a></h3>\n\n</div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\Bikram\Desktop\TEMP\sodveloncon2019\src\pages\download\download.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["o" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["s" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1__providers_user_data__["a" /* UserData */],
            __WEBPACK_IMPORTED_MODULE_0__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["AngularFireDatabase"], __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["p" /* NavParams */], __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["e" /* Events */]])
    ], DownloadPage);
    return DownloadPage;
}());

//# sourceMappingURL=download.js.map

/***/ }),

/***/ 177:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 177;

/***/ }),

/***/ 222:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/about/about.module": [
		752,
		9
	],
	"../pages/download/download.module": [
		751,
		10
	],
	"../pages/login/login.module": [
		753,
		8
	],
	"../pages/map/map.module": [
		756,
		7
	],
	"../pages/schedule-filter/schedule-filter.module": [
		754,
		6
	],
	"../pages/schedule/schedule.module": [
		758,
		5
	],
	"../pages/session-detail/session-detail.module": [
		755,
		4
	],
	"../pages/speaker-detail/speaker-detail.module": [
		757,
		3
	],
	"../pages/speaker-list/speaker-list.module": [
		760,
		2
	],
	"../pages/tabs/tabs.module": [
		759,
		1
	],
	"../pages/tutorial/tutorial.module": [
		761,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 222;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 369:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AccountPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_user_data__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_barcode_scanner__ = __webpack_require__(370);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};


// import { LoginPage } from '../login/login';
// import { SupportPage } from '../support/support';


var AccountPage = /** @class */ (function () {
    function AccountPage(alertCtrl, nav, userData, barcodeScanner) {
        this.alertCtrl = alertCtrl;
        this.nav = nav;
        this.userData = userData;
        this.barcodeScanner = barcodeScanner;
    }
    //   createCode()
    // {
    //   this.createdCode = this.username
    // }
    // ngOnInit(): void {
    //   //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //   //Add 'implements OnInit' to the class.
    //   this.createdCode = this.username
    // }
    //   // scan(){
    //   this.options = {
    //     prompt: 'Scan your bar code'
    //   };
    //   this.scanner.scan(this.options).then((data)=>{
    //     this.scannedData = data;
    //   },(err)=>{
    //     console.log('Error:', err);
    //   })
    // }
    // encode(){
    //   this.scanner.encode(this.scanner.Encode.TEXT_TYPE, this.username).then((data)=>{
    //     this.encodedData = data;
    //   },(err)=>{
    //     console.log('Error:', err);
    //   })
    // }
    AccountPage.prototype.ngAfterViewInit = function () {
        this.getUsername();
    };
    // updatePicture() {
    //   console.log('Clicked to update picture');
    // }
    // Present an alert with the current username populated
    // clicking OK will update the username and display it
    // clicking Cancel will close the alert and do nothing
    // changeUsername() {
    //   let alert = this.alertCtrl.create({
    //     title: 'Change Username',
    //     buttons: [
    //       'Cancel'
    //     ]
    //   });
    //   alert.addInput({
    //     name: 'username',
    //     value: this.username,
    //     placeholder: 'username'
    //   });
    //   alert.addButton({
    //     text: 'Ok',
    //     handler: (data: any) => {
    //       this.userData.setUsername(data.username);
    //       this.getUsername();
    //     }
    //   });
    //   alert.present();
    // }
    AccountPage.prototype.getUsername = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.userData.getUsername().then(function (username) {
                    _this.username = username;
                    console.log(_this.username);
                });
                return [2 /*return*/];
            });
        });
    };
    AccountPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-account',template:/*ion-inline-start:"C:\Users\Bikram\Desktop\TEMP\sodveloncon2019\src\pages\account\account.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>Account</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content class="outer-content">\n\n  <!-- <div padding-top text-center *ngIf="username">\n\n    <h2>Welcome, {{username}}</h2>\n\n    <h4>For support click here!</h4>\n\n\n\n    <ion-list inset>\n\n      \n\n      <button ion-button block (click)="support()">Support</button>\n\n      <p>Click Show QR Code button when you enter Sodveloncon2019 Conference at Registration Desk, Thank you for your patience!</p>\n\n      <button ion-button block (click)="createCode()"><ion-icon name="expand"></ion-icon>Show QR Code</button>\n\n    \n\n    </ion-list>\n\n  </div> -->\n\n\n\n  <p style="text-align: center;color: rgb(49, 49, 110);font-size: 15px">Please Kindly show QR Code at SODVELONCON2019 Conference when necessary, Thank you!</p>\n\n  <ion-card *ngIf="username">\n\n      \n\n      \n\n        <ngx-qrcode [qrc-value]="username" ></ngx-qrcode>\n\n      \n\n      <ion-card-content class="card-mytext">\n\n        <p style="text-align: center;color: rgb(49, 49, 110);font-size: 20px">QR Code - SODVELONCON2019</p>\n\n        <p style="text-align: center; font-weight: bold">{{ username }}</p>\n\n      </ion-card-content>\n\n    \n\n  </ion-card>\n\n\n\n  <!-- <div>\n\n    <ion-input style="background-color: gray" type="text" [(ngModel)] = "encodeText"></ion-input>\n\n    <button ion-button block (click)="encode()">Show Bar Code</button>\n\n  </div>\n\n  <div>\n\n    <button ion-button block (click)="scan()">Scan</button>\n\n    <div *ngIf="scannedData.text">\n\n      <label>Your Barcode is</label>\n\n      <br>\n\n      <span>{{scannedData.text}}</span>\n\n    </div>\n\n  </div> -->\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\Bikram\Desktop\TEMP\sodveloncon2019\src\pages\account\account.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_user_data__["a" /* UserData */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_barcode_scanner__["a" /* BarcodeScanner */]])
    ], AccountPage);
    return AccountPage;
}());

//# sourceMappingURL=account.js.map

/***/ }),

/***/ 378:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(379);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(383);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 383:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(726);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_about_popover_about_popover__ = __webpack_require__(727);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_account_account__ = __webpack_require__(369);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_support_support__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_download_download__ = __webpack_require__(166);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ionic3_star_rating__ = __webpack_require__(728);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_conference_data__ = __webpack_require__(165);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_user_data__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_in_app_browser__ = __webpack_require__(377);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_splash_screen__ = __webpack_require__(368);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__angular_platform_browser__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__angular_http__ = __webpack_require__(327);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_angularfire2__ = __webpack_require__(730);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_angularfire2___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_15_angularfire2__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16_angularfire2_auth__ = __webpack_require__(376);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_16_angularfire2_auth__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17_angularfire2_database__ = __webpack_require__(121);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17_angularfire2_database___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_17_angularfire2_database__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__ionic_native_barcode_scanner__ = __webpack_require__(370);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19_ngx_qrcode2__ = __webpack_require__(731);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






// import { SignupPage } from '../pages/signup/signup';














var firebaseConfig = {
    firebase: {
        apiKey: "AIzaSyAoYcfP5NVElj1FhafipljiS9NHLXc70rM",
        authDomain: "sodveloncon18.firebaseapp.com",
        databaseURL: "https://sodveloncon18.firebaseio.com",
        projectId: "sodveloncon18",
        storageBucket: "sodveloncon18.appspot.com",
        messagingSenderId: "430886862725"
    },
    users_endpoint: "users",
    chats_endpoint: "chats"
};
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* ConferenceApp */],
                __WEBPACK_IMPORTED_MODULE_5__pages_account_account__["a" /* AccountPage */],
                __WEBPACK_IMPORTED_MODULE_4__pages_about_popover_about_popover__["a" /* PopoverPage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_support_support__["a" /* SupportPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_download_download__["a" /* DownloadPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* ConferenceApp */], {}, {
                    links: [
                        { loadChildren: '../pages/download/download.module#DownloadPageModule', name: 'DownloadPage', segment: 'download', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/about/about.module#AboutPageModule', name: 'AboutPage', segment: 'about', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/schedule-filter/schedule-filter.module#ScheduleFilterPageModule', name: 'ScheduleFilterPage', segment: 'schedule-filter', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/session-detail/session-detail.module#SessionDetailPageModule', name: 'SessionDetailPage', segment: 'session-detail', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/map/map.module#MapPageModule', name: 'MapPage', segment: 'map', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/speaker-detail/speaker-detail.module#SpeakerDetailPageModule', name: 'SpeakerDetailPage', segment: 'speaker-detail', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/schedule/schedule.module#SchedulePageModule', name: 'SchedulePage', segment: 'schedule', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/tabs/tabs.module#TabsPageModule', name: 'TabsPage', segment: 'tabs', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/speaker-list/speaker-list.module#SpeakerListPageModule', name: 'SpeakerListPage', segment: 'speaker-list', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/tutorial/tutorial.module#TutorialPageModule', name: 'TutorialPage', segment: 'tutorial', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_15_angularfire2__["AngularFireModule"].initializeApp(firebaseConfig.firebase),
                __WEBPACK_IMPORTED_MODULE_13__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_14__angular_http__["b" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_16_angularfire2_auth__["AngularFireAuthModule"],
                __WEBPACK_IMPORTED_MODULE_17_angularfire2_database__["AngularFireDatabaseModule"],
                __WEBPACK_IMPORTED_MODULE_19_ngx_qrcode2__["a" /* NgxQRCodeModule */],
                __WEBPACK_IMPORTED_MODULE_8_ionic3_star_rating__["a" /* StarRatingModule */],
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* ConferenceApp */],
                __WEBPACK_IMPORTED_MODULE_5__pages_account_account__["a" /* AccountPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_download_download__["a" /* DownloadPage */],
                __WEBPACK_IMPORTED_MODULE_4__pages_about_popover_about_popover__["a" /* PopoverPage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_support_support__["a" /* SupportPage */]
            ],
            providers: [
                { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_9__providers_conference_data__["a" /* ConferenceData */],
                __WEBPACK_IMPORTED_MODULE_10__providers_user_data__["a" /* UserData */],
                __WEBPACK_IMPORTED_MODULE_11__ionic_native_in_app_browser__["a" /* InAppBrowser */],
                __WEBPACK_IMPORTED_MODULE_12__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_18__ionic_native_barcode_scanner__["a" /* BarcodeScanner */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 47:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserData; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(51);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var UserData = /** @class */ (function () {
    function UserData(events, storage) {
        this.events = events;
        this.storage = storage;
        this._favorites = [];
        this.HAS_LOGGED_IN = 'hasLoggedIn';
        this.HAS_SEEN_TUTORIAL = 'hasSeenTutorial';
        this.HAS_RATE_APP = 'hasRateApp';
    }
    UserData.prototype.hasFavorite = function (sessionName) {
        return (this._favorites.indexOf(sessionName) > -1);
    };
    ;
    UserData.prototype.addFavorite = function (sessionName) {
        this._favorites.push(sessionName);
    };
    ;
    UserData.prototype.removeFavorite = function (sessionName) {
        var index = this._favorites.indexOf(sessionName);
        if (index > -1) {
            this._favorites.splice(index, 1);
        }
    };
    ;
    UserData.prototype.login = function (username) {
        this.storage.set(this.HAS_LOGGED_IN, true);
        this.setUsername(username);
        this.events.publish('user:login');
    };
    ;
    UserData.prototype.signup = function (username) {
        this.storage.set(this.HAS_LOGGED_IN, true);
        this.setUsername(username);
        this.events.publish('user:signup');
    };
    ;
    UserData.prototype.logout = function () {
        this.storage.remove(this.HAS_LOGGED_IN);
        this.storage.remove('username');
        this.events.publish('user:logout');
    };
    ;
    UserData.prototype.setUsername = function (username) {
        this.storage.set('username', username);
    };
    ;
    UserData.prototype.getUsername = function () {
        return this.storage.get('username').then(function (value) {
            return value;
        });
    };
    ;
    UserData.prototype.hasLoggedIn = function () {
        return this.storage.get(this.HAS_LOGGED_IN).then(function (value) {
            return value === true;
        });
    };
    ;
    UserData.prototype.checkHasSeenTutorial = function () {
        return this.storage.get(this.HAS_SEEN_TUTORIAL).then(function (value) {
            return value;
        });
    };
    ;
    UserData.prototype.setRateStatus = function (ratestatus) {
        this.storage.set('ratestatus', ratestatus);
    };
    UserData.prototype.checkHasRateApp = function () {
        return this.storage.get('ratestatus').then(function (value) {
            return value;
        });
    };
    ;
    UserData = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Events */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */]])
    ], UserData);
    return UserData;
}());

//# sourceMappingURL=user-data.js.map

/***/ }),

/***/ 726:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConferenceApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_splash_screen__ = __webpack_require__(368);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_account_account__ = __webpack_require__(369);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_support_support__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_download_download__ = __webpack_require__(166);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_conference_data__ = __webpack_require__(165);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_user_data__ = __webpack_require__(47);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var ConferenceApp = /** @class */ (function () {
    function ConferenceApp(events, userData, menu, platform, confData, storage, splashScreen) {
        var _this = this;
        this.events = events;
        this.userData = userData;
        this.menu = menu;
        this.platform = platform;
        this.confData = confData;
        this.storage = storage;
        this.splashScreen = splashScreen;
        // List of pages that can be navigated to from the left menu
        // the left menu only works after login
        // the login page disables the left menu
        this.appPages = [
            { title: 'Schedule', component: "TabsPage", tabComponent: "SchedulePage", icon: 'calendar' },
            { title: 'Speakers', component: "TabsPage", tabComponent: "SpeakerListPage", index: 1, icon: 'contacts' },
            { title: 'Map', component: "TabsPage", tabComponent: "MapPage", index: 2, icon: 'map' },
            { title: 'Downloads', component: __WEBPACK_IMPORTED_MODULE_6__pages_download_download__["a" /* DownloadPage */], tabComponent: __WEBPACK_IMPORTED_MODULE_6__pages_download_download__["a" /* DownloadPage */], index: 4, icon: 'download' },
            { title: 'About', component: "TabsPage", tabComponent: "AboutPage", index: 3, icon: 'information-circle' }
        ];
        this.loggedInPages = [
            { title: 'Show QR Code', component: __WEBPACK_IMPORTED_MODULE_4__pages_account_account__["a" /* AccountPage */], icon: 'qr-scanner' },
            { title: 'Support', component: __WEBPACK_IMPORTED_MODULE_5__pages_support_support__["a" /* SupportPage */], icon: 'help' },
        ];
        this.loggedOutPages = [
            { title: 'Show QR Code', component: __WEBPACK_IMPORTED_MODULE_4__pages_account_account__["a" /* AccountPage */], icon: 'qr-scanner' },
            //{ title: 'Login', component: LoginPage, icon: 'log-in' },
            { title: 'Support', component: __WEBPACK_IMPORTED_MODULE_5__pages_support_support__["a" /* SupportPage */], icon: 'help' },
        ];
        this.rootPage = "";
        // Check if the user has already seen the tutorial
        this.storage.get('hasSeenTutorial')
            .then(function (hasSeenTutorial) {
            if (hasSeenTutorial) {
                _this.rootPage = "LoginPage";
            }
            else {
                _this.rootPage = "TutorialPage";
            }
            _this.platformReady();
        });
        // load the conference data
        confData.load();
        // decide which menu items should be hidden by current login status stored in local storage
        this.userData.hasLoggedIn().then(function (hasLoggedIn) {
            _this.enableMenu(hasLoggedIn === true);
        });
        this.listenToLoginEvents();
    }
    ConferenceApp.prototype.openPage = function (page) {
        var _this = this;
        // the nav component was found using @ViewChild(Nav)
        // reset the nav to remove previous pages and only have this page
        // we wouldn't want the back button to show in this scenario
        if (page.index) {
            this.nav.setRoot(page.component, { tabIndex: page.index }).catch(function () {
                console.log("Didn't set nav root");
            });
        }
        else {
            this.nav.setRoot(page.component).catch(function () {
                console.log("Didn't set nav root");
            });
        }
        if (page.logsOut === true) {
            // Give the menu time to close before changing to logged out
            setTimeout(function () {
                _this.userData.logout();
            }, 1000);
        }
    };
    ConferenceApp.prototype.openTutorial = function () {
        this.nav.setRoot("TutorialPage");
    };
    ConferenceApp.prototype.listenToLoginEvents = function () {
        var _this = this;
        this.events.subscribe('user:login', function () {
            _this.enableMenu(true);
        });
        this.events.subscribe('user:signup', function () {
            _this.enableMenu(true);
        });
        this.events.subscribe('user:logout', function () {
            _this.enableMenu(false);
        });
    };
    ConferenceApp.prototype.enableMenu = function (loggedIn) {
        this.menu.enable(loggedIn, 'loggedInMenu');
        this.menu.enable(!loggedIn, 'loggedOutMenu');
    };
    ConferenceApp.prototype.platformReady = function () {
        var _this = this;
        // Call any initial plugins when ready
        this.platform.ready().then(function () {
            _this.splashScreen.hide();
        });
    };
    ConferenceApp.prototype.isActive = function (page) {
        var childNav = this.nav.getActiveChildNav();
        // Tabs are a special case because they have their own navigation
        if (childNav) {
            if (childNav.getSelected() && childNav.getSelected().root === page.tabComponent) {
                return 'primary';
            }
            return;
        }
        if (this.nav.getActive() && this.nav.getActive().component === page.component) {
            return 'primary';
        }
        return;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Nav */])
    ], ConferenceApp.prototype, "nav", void 0);
    ConferenceApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\Bikram\Desktop\TEMP\sodveloncon2019\src\app\app.template.html"*/'<ion-split-pane>\n\n\n\n  <!-- logged out menu -->\n\n  <ion-menu id="loggedOutMenu" [content]="content">\n\n\n\n    <ion-header>\n\n      <ion-toolbar>\n\n        <ion-title>Menu</ion-title>\n\n      </ion-toolbar>\n\n    </ion-header>\n\n\n\n    <ion-content class="outer-content">\n\n\n\n      <ion-list>\n\n        <ion-list-header>\n\n          Navigate\n\n        </ion-list-header>\n\n        <button ion-item menuClose *ngFor="let p of appPages" (click)="openPage(p)">\n\n          <ion-icon item-left [name]="p.icon" [color]="isActive(p)"></ion-icon>\n\n          {{p.title}}\n\n        </button>\n\n      </ion-list>\n\n\n\n      <ion-list>\n\n        <ion-list-header>\n\n          Account\n\n        </ion-list-header>\n\n        <button ion-item menuClose *ngFor="let p of loggedOutPages" (click)="openPage(p)">\n\n          <ion-icon item-left [name]="p.icon" [color]="isActive(p)"></ion-icon>\n\n          {{p.title}}\n\n        </button>\n\n      </ion-list>\n\n\n\n      <ion-list>\n\n        <ion-list-header>\n\n          Tutorial\n\n        </ion-list-header>\n\n        <button ion-item menuClose (click)="openTutorial()">\n\n          <ion-icon item-left name="hammer"></ion-icon>\n\n          Show Tutorial\n\n        </button>\n\n        <br>    \n\n          <div text-center class="bitpoint">\n\n            <a href="https://www.bitpointnepal.com"><img style="width: 30px;align-self: center;" src="assets/img/bitpoint.png"/>  </a>\n\n         </div>\n\n        <p style="color: black;font-size: 13px;text-align: center">Powered by<br /> Bitpoint Pvt. Ltd.<br />bitpointnepal@gmail.com</p>\n\n        \n\n      </ion-list>\n\n    </ion-content>\n\n\n\n  </ion-menu>\n\n\n\n  <!-- logged in menu -->\n\n  <ion-menu id="loggedInMenu" [content]="content">\n\n\n\n    <ion-header>\n\n      <ion-toolbar>\n\n        <ion-title>Menu</ion-title>\n\n      </ion-toolbar>\n\n    </ion-header>\n\n\n\n    <ion-content class="outer-content">\n\n\n\n      <ion-list>\n\n        <ion-list-header>\n\n          Navigate\n\n        </ion-list-header>\n\n        <button ion-item menuClose *ngFor="let p of appPages" (click)="openPage(p)">\n\n          <ion-icon item-left [name]="p.icon" [color]="isActive(p)"></ion-icon>\n\n          {{p.title}}\n\n        </button>\n\n      </ion-list>\n\n\n\n      <ion-list>\n\n        <ion-list-header>\n\n          Account\n\n        </ion-list-header>\n\n        <button ion-item menuClose *ngFor="let p of loggedInPages" (click)="openPage(p)">\n\n          <ion-icon item-left [name]="p.icon" [color]="isActive(p)"></ion-icon>\n\n          {{p.title}}\n\n        </button>\n\n        \n\n      </ion-list>\n\n\n\n      <ion-list>\n\n        <ion-list-header>\n\n          Tutorial\n\n        </ion-list-header>\n\n        <button ion-item menuClose (click)="openTutorial()">\n\n          <ion-icon item-left name="hammer"></ion-icon>\n\n          Show Tutorial\n\n        </button>\n\n        <br>\n\n        <div text-center>\n\n          <img style="width: 30px;align-self: center;" src="assets/img/bitpoint.png"/>  \n\n       </div>\n\n      <p style="color: black;font-size: 13px;text-align: center">Powered by<br /> Bitpoint Pvt. Ltd.<br />bitpointnepal@gmail.com</p>\n\n      \n\n      </ion-list>\n\n\n\n    </ion-content>\n\n\n\n    \n\n\n\n  </ion-menu>\n\n\n\n  <!-- main navigation -->\n\n  <ion-nav [root]="rootPage" #content swipeBackEnabled="false" main></ion-nav>\n\n\n\n</ion-split-pane>\n\n'/*ion-inline-end:"C:\Users\Bikram\Desktop\TEMP\sodveloncon2019\src\app\app.template.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Events */],
            __WEBPACK_IMPORTED_MODULE_8__providers_user_data__["a" /* UserData */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_7__providers_conference_data__["a" /* ConferenceData */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], ConferenceApp);
    return ConferenceApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 727:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PopoverPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__support_support__ = __webpack_require__(162);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PopoverPage = /** @class */ (function () {
    function PopoverPage(viewCtrl, navCtrl, app, modalCtrl) {
        this.viewCtrl = viewCtrl;
        this.navCtrl = navCtrl;
        this.app = app;
        this.modalCtrl = modalCtrl;
    }
    PopoverPage.prototype.support = function () {
        this.app.getRootNav().push(__WEBPACK_IMPORTED_MODULE_2__support_support__["a" /* SupportPage */]);
        this.viewCtrl.dismiss();
    };
    PopoverPage.prototype.close = function (url) {
        window.open(url, '_blank');
        this.viewCtrl.dismiss();
    };
    PopoverPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            template: "\n    <ion-list>\n      <button ion-item (click)=\"close('http://ionicframework.com/docs/v2/getting-started')\">Learn Ionic</button>\n      <button ion-item (click)=\"close('http://ionicframework.com/docs/v2')\">Documentation</button>\n      <button ion-item (click)=\"close('http://showcase.ionicframework.com')\">Showcase</button>\n      <button ion-item (click)=\"close('https://github.com/driftyco/ionic')\">GitHub Repo</button>\n      <button ion-item (click)=\"support()\">Support</button>\n    </ion-list>\n  "
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* App */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ModalController */]])
    ], PopoverPage);
    return PopoverPage;
}());

//# sourceMappingURL=about-popover.js.map

/***/ })

},[378]);
//# sourceMappingURL=main.js.map