import { SpeakerDetailPage } from './speaker-detail';
import { IonicPageModule } from 'ionic-angular';
import { NgModule } from '@angular/core';


@NgModule({
    declarations: [
        SpeakerDetailPage,
    ],
    imports: [
        IonicPageModule.forChild(SpeakerDetailPage),
    ],
    exports: [
        SpeakerDetailPage
    ]
})

export class SpeakerDetailPageModule {}