import { Component } from '@angular/core';

import { NavParams, MenuController, IonicPage } from 'ionic-angular';


@IonicPage()
@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  // set the root pages for each tab
  tab1Root: string = "SchedulePage";
  tab2Root: string = "SpeakerListPage";
  tab3Root: string = "MapPage";
  tab4Root: any = "AboutPage";
  mySelectedIndex: number;

  constructor(private menu: MenuController,
    navParams: NavParams) {
    this.mySelectedIndex = navParams.data.tabIndex || 0;
  }

  ionViewCanEnter(){
    this.menu.enable(true);
   
  }

}
