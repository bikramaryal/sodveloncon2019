import { Storage } from '@ionic/storage';
import { UserData } from './../../providers/user-data';
import { AngularFireDatabase } from 'angularfire2/database';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, ToastOptions } from 'ionic-angular';
import { Events } from 'ionic-angular';
import { Md5 } from 'ts-md5/dist/md5';

/**
 * Generated class for the DownloadPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-download',
  templateUrl: 'download.html',
})
export class DownloadPage {
  count: number = 1;
  rating: number = 4;
  questionName: String;
  username: String;
  toastOptions: ToastOptions;
  eventDate = new Date('2019-04-08');
  electionDate = new Date('2019-04-07');
  rateStatus: String = 'notDone';
  hashString: String;
  hashUsername: string;
  
  today = Date.now();
  constructor(public navCtrl: NavController,
    public toastCtrl: ToastController,
    public userData: UserData,
    public storage: Storage,
    public db:AngularFireDatabase, public navParams: NavParams, public events1: Events) {

    events1.subscribe('star-rating:changed', (starRating) => {
      console.log(starRating);
      this.rating = starRating;
    });

  
  }

  ngAfterViewInit() {
    this.getRateStatus();
  }

  

  nextRating(){
    this.userData.getUsername().then((username) => {
      this.username = username;
      this.hashUsername = username;
      this.db.list('/rating').push({
        username: this.username,
        question: this.count,
        ratingValue: this.rating
      }).then(() => {
        if(this.count == 5){
          this.Submit();
          this.userData.setRateStatus('done');
        }
        this.count++;
      })
      console.log("Some error occured")
      
    })
  }

  

  // previousRating(){
  //   console.log("Previous Rating button clicked");
  //   this.count--;
  //   }

  

  ionViewDidLoad() {
    console.log('ionViewDidLoad DownloadPage');
    this.userData.getUsername().then((username) => {
      this.hashString = String(Md5.hashStr(username));
    })
    
    
  }

  
  
 

  Submit(){
    console.log("Submit Successful");
    this.toastOptions = {
      message: "Review has been recorded! Thank you!",
      duration: 3000
    }
    this.toastCtrl.create(this.toastOptions).present();
  }

  async getRateStatus(){
    this.userData.checkHasRateApp().then((rateStatus) => {
      this.rateStatus = rateStatus;
    })
  }
  

}
