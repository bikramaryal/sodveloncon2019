webpackJsonp([9],{

/***/ 752:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutPageModule", function() { return AboutPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__about__ = __webpack_require__(762);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AboutPageModule = /** @class */ (function () {
    function AboutPageModule() {
    }
    AboutPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_0__about__["a" /* AboutPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_0__about__["a" /* AboutPage */]),
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_0__about__["a" /* AboutPage */]
            ]
        })
    ], AboutPageModule);
    return AboutPageModule;
}());

//# sourceMappingURL=about.module.js.map

/***/ }),

/***/ 762:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

// import { PopoverPage } from '../about-popover/about-popover';
var AboutPage = /** @class */ (function () {
    function AboutPage() {
        this.conferenceDate = '2019-04-07';
    }
    AboutPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-about',template:/*ion-inline-start:"C:\Users\Bikram\Desktop\TEMP\sodveloncon2019\src\pages\about\about.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>About</ion-title>\n\n    <ion-buttons end>\n\n      <!-- <button ion-button icon-only (click)="presentPopover($event)">\n\n        <ion-icon name="more"></ion-icon>\n\n      </button> -->\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <div class="about-header">\n\n    <img src="assets/img/39395252_253192841971744_3602402840267456512_n.jpg" alt="ionic logo">\n\n  </div>\n\n  <div padding class="about-info">\n\n    <h3 style="font-family: \'Segoe UI\', Tahoma, Geneva, Verdana, sans-serif;text-align: center">SODVELONCON2019</h3>\n\n\n\n    <ion-list no-lines>\n\n      <ion-item>\n\n        <ion-icon name="calendar" item-left></ion-icon>\n\n        <ion-label>Date</ion-label>\n\n        <ion-datetime displayFormat="MMM DD, YYYY" max="2018" [(ngModel)]="conferenceDate"></ion-datetime>\n\n      </ion-item>\n\n\n\n      <ion-item>\n\n        <ion-icon name="pin" item-left></ion-icon>\n\n        <ion-label>Location</ion-label>\n\n        <ion-select>\n\n          <ion-option value="madison" selected>Crowne Plaza Kathmandu - Soaltee</ion-option>\n\n          \n\n        </ion-select>\n\n      </ion-item>\n\n    </ion-list>\n\n\n\n    <p>\n\n      The 15th National Conference of the Society of Dermatologists, Venereologists and Leprologists of Nepal, the SODVELONCON 2019 is going to be held in Kathmandu on April 7-8, 2019. It is a matter of great honour and privilege for the Department of Dermatology & Venereology, Bir Hospital, on being given the opportunity to organize SODVELONCON 2019. The Organizing Committee is working very hard coupled with great enthusiasm and efficient planning to make this mega event a grand success. We are expecting eminent national and international speakers, who will be highlighting their experiences in day-to-day practice and update our knowledge on recent advances in cutaneous medicine and aesthetic procedures.\n\n    </p>\n\n  </div>\n\n\n\n  <div class="team">\n\n    <img src="assets/img/team.png" alt="team info">\n\n  </div>\n\n\n\n\n\n  <p style="font-weight: bold; font-size: 15px">Scientific Sub-Committee Members</p>\n\n  <ul>\n\n    <li style="font-size: 11px">Dr Yogesh Poudyal</li>\n\n    <li style="font-size: 11px">Dr Anup Bastola</li>\n\n    <li style="font-size: 11px">Dr Badri Chapagain</li>\n\n    <li style="font-size: 11px">Dr Rabindra Baskota</li>\n\n    <li style="font-size: 11px">Dr Pratima Poudel</li>\n\n    <li style="font-size: 11px">Dr Nashwa Ahmed</li>\n\n \n\n    \n\n  </ul>\n\n\n\n\n\n    \n\n  \n\n'/*ion-inline-end:"C:\Users\Bikram\Desktop\TEMP\sodveloncon2019\src\pages\about\about.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], AboutPage);
    return AboutPage;
}());

//# sourceMappingURL=about.js.map

/***/ })

});
//# sourceMappingURL=9.js.map