import { TutorialPage } from './tutorial';
import { IonicPageModule } from 'ionic-angular';
import { NgModule } from '@angular/core';


@NgModule({
    declarations: [
        TutorialPage,
    ],
    imports: [
        IonicPageModule.forChild(TutorialPage),
    ],
    exports: [
        TutorialPage
    ]
})

export class TutorialPageModule {}