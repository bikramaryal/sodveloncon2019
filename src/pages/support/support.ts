
//   selector: 'page-user',
//   templateUrl: 'support.html'
// })
// export class SupportPage {

//   submitted: boolean = false;
//   supportMessage: string;

//   constructor(
//     public navCtrl: NavController,
//     public alertCtrl: AlertController,
//     public toastCtrl: ToastController
//   ) {

//   }

//   ionViewDidEnter() {
//     let toast = this.toastCtrl.create({
//       message: 'This does not actually send a support request.',
//       duration: 3000
//     });
//     toast.present();
//   }



//   // If the user enters text in the support question and then navigates
//   // without submitting first, ask if they meant to leave the page
//   ionViewCanLeave(): boolean | Promise<boolean> {
//     // If the support message is empty we should just navigate
//     if (!this.supportMessage || this.supportMessage.trim().length === 0) {
//       return true;
//     }

//     return new Promise((resolve: any, reject: any) => {
//       let alert = this.alertCtrl.create({
//         title: 'Leave this page?',
//         message: 'Are you sure you want to leave this page? Your support message will not be submitted.'
//       });
//       alert.addButton({ text: 'Stay', handler: reject });
//       alert.addButton({ text: 'Leave', role: 'cancel', handler: resolve });

//       alert.present();
//     });
//   }
// }



import { Component } from '@angular/core';
import { NavParams, NavController, ToastController, ToastOptions } from 'ionic-angular';
import { AngularFireDatabase } from 'angularfire2/database';
import { UserData } from '../../providers/user-data';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-support',
  templateUrl: 'support.html',
})
export class SupportPage {

  username: string;
  message: string = '';
  _chatSubscription: any;
  messages: object;
  submitted = false;
  toastOptions: ToastOptions;



  constructor(public navParam: NavParams,
    public db: AngularFireDatabase,
    private toastCtrl: ToastController,
    public navCtrl: NavController,public userData: UserData,public storage: Storage) {
    // console.log(this.storage.get('username'))
    // this.userData.getUsername().then((username) => {
    //   this.username = username;
    // }); 
    // this.getUsername();
    //console.log(this.username)
    this._chatSubscription = this.db.object('/chat').valueChanges().subscribe( data => {
      console.log(data);
    });

  }
  


  sendMessage() {
    this.userData.getUsername().then((username) => {
      this.username = username;
      console.log(this.username);
      if(this.message.length > 15){
      this.db.list('/chat').push({
      username: this.username,
      message: this.message
       })
      }
    }).then( ()=> {
      
      if(this.message.length > 15){
        this.toastOptions = {
          message: "Your support message has been sent successfully",
          duration: 3000
        }
        this.toastCtrl.create(this.toastOptions).present();
      }
      else{
        this.toastOptions = {
          message: "Unable to send support Request!",
          duration: 3000
        }
        this.toastCtrl.create(this.toastOptions).present();

      }

        
        
        
      
      
      //message is sent
      this.message = "";
      this.submitted = true;
    }).catch( ()=> {
      //some error maybe firebase unreachable

    });
  }

  // sendMessage(){ this.db.list('/chat').push({
  //   username: this.username,
  //   message: this.message
  // })

  ionViewDidLoad() {

    // this.db.list('/chat').push({
    //   specialMessage: true,
    //   message: `${this.username} has joined the room`
    // });
  }
  ionViewWillLeave(){
    // this._chatSubscription.unsubscribe();
    // this.db.list('/chat').push({
    //   specialMessage: true,
    //   message: `${this.username} has left the room`
    // });
  }
 
  }




