import { SessionDetailPage } from './session-detail';
import { IonicPageModule } from 'ionic-angular';
import { NgModule } from '@angular/core';


@NgModule({
    declarations: [
        SessionDetailPage,
    ],
    imports: [
        IonicPageModule.forChild(SessionDetailPage),
    ],
    exports: [
        SessionDetailPage
    ]
})

export class SessionDetailPageModule {}