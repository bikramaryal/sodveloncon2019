import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';

import { NavController, ToastOptions, LoadingController, IonicPage } from 'ionic-angular';

// import { SignupPage } from '../signup/signup';
// import { SupportPage } from '../../pages/support/support'
import { UserData } from '../../providers/user-data';
import { AngularFireAuth } from "angularfire2/auth";

import { ToastController,MenuController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-user',
  templateUrl: 'login.html'
})
export class LoginPage {
  login: {username?: string, password?: string} = {};
  submitted = false;

  toastOptions: ToastOptions

  goToTabsPage() {
    this.userData.getUsername().then((username) => {
      this.login.username = username;
      console.log(this.login.username);
      if(this.login.username){
        this.navCtrl.push("TabsPage");
      }
    }).then( ()=> {
      //message is sent
    }).catch( ()=> {
      //some error maybe firebase unreachable

    });
  }

  constructor(private loadingCtrl: LoadingController,
    private afAuth: AngularFireAuth,
  private toastCtrl: ToastController,
  public navCtrl: NavController,
  public menu: MenuController,
  public userData: UserData) {

    this.menu.enable(false);
    // if(this.submitted == false){
      this.goToTabsPage();
    // }
    
    
   }
    async onLogin(form: NgForm) {
    this.submitted = true;

    

    if (form.valid) {
      let loader = this.loadingCtrl.create({
        content: 'please wait'
      })
       
      try{
      
      loader.present();
      const result = await this.afAuth.auth.signInWithEmailAndPassword(this.login.username,this.login.password);
      console.log(result);
      loader.dismiss();
      if(result) {

        this.toastOptions = {
          message: "Login Successful, Welcome  " + this.login.username,
          duration: 3000
        }
        this.toastCtrl.create(this.toastOptions).present();
        // this.navCtrl.push(SupportPage, {
        //   user: this.login.username
        // });
        this.userData.login(this.login.username);
        this.navCtrl.push("TabsPage")
        }
        this.submitted = true;
    
      }
      catch(e){
        loader.dismiss();
        this.submitted = false;

       // this.userData.login(this.login.username);
        this.toastOptions = {
          message: "Login failed! Check your username and password",
          duration: 3000
        }
        this.toastCtrl.create(this.toastOptions).present();
        
        console.error(e);
      }
      
    }
    
  }
  

  // onSignup() {
  //   this.navCtrl.push(SignupPage);
  // }

  ionViewDidEnter() {
    // the root left menu should be disabled on the tutorial page
    this.menu.enable(false);
  }

  // ionViewDidLeave() {
  //   // enable the root left menu when leaving the tutorial page
  //   this.menu.enable(true);
  // }

  
}
