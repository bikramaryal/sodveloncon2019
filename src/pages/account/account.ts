import { Component } from '@angular/core';

import { AlertController, NavController } from 'ionic-angular';

// import { LoginPage } from '../login/login';
// import { SupportPage } from '../support/support';
import { UserData } from '../../providers/user-data';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner';


@Component({
  selector: 'page-account',
  templateUrl: 'account.html'
})
export class AccountPage {
  // options: BarcodeScannerOptions;
  // encodeText: string='';
  // encodedData:any={};
  // scannedData:any={};
  //createdCode : string = null
  username: string;

  constructor(public alertCtrl: AlertController, public nav: NavController,
     public userData: UserData,
     public barcodeScanner: BarcodeScanner
     ) {

  }

//   createCode()
// {
//   this.createdCode = this.username
// }
// ngOnInit(): void {
//   //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
//   //Add 'implements OnInit' to the class.
//   this.createdCode = this.username
// }
//   // scan(){
  //   this.options = {
  //     prompt: 'Scan your bar code'
  //   };
  //   this.scanner.scan(this.options).then((data)=>{
  //     this.scannedData = data;
  //   },(err)=>{
  //     console.log('Error:', err);
  //   })
  // }
  // encode(){
  //   this.scanner.encode(this.scanner.Encode.TEXT_TYPE, this.username).then((data)=>{
  //     this.encodedData = data;
  //   },(err)=>{
  //     console.log('Error:', err);
  //   })

  // }

  ngAfterViewInit() {
    this.getUsername();
    
  }

  // updatePicture() {
  //   console.log('Clicked to update picture');
  // }

  // Present an alert with the current username populated
  // clicking OK will update the username and display it
  // clicking Cancel will close the alert and do nothing
  // changeUsername() {
  //   let alert = this.alertCtrl.create({
  //     title: 'Change Username',
  //     buttons: [
  //       'Cancel'
  //     ]
  //   });
  //   alert.addInput({
  //     name: 'username',
  //     value: this.username,
  //     placeholder: 'username'
  //   });
  //   alert.addButton({
  //     text: 'Ok',
  //     handler: (data: any) => {
  //       this.userData.setUsername(data.username);
  //       this.getUsername();
  //     }
  //   });

  //   alert.present();
  // }

  async getUsername() {
    this.userData.getUsername().then((username) => {
      this.username = username;
      console.log(this.username);
    });
  }

  // changePassword() {
  //   console.log('Clicked to change password');
  // }

  // logout() {
  //   this.userData.logout();
  //   this.nav.setRoot(LoginPage);
  // }

  // support() {
  //   this.nav.push(SupportPage);
  // }
}
