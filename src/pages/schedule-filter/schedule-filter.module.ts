import { ScheduleFilterPage } from './schedule-filter';
import { IonicPageModule } from 'ionic-angular';
import { NgModule } from '@angular/core';


@NgModule({
    declarations: [
        ScheduleFilterPage,
    ],
    imports: [
        IonicPageModule.forChild(ScheduleFilterPage),
    ],
    exports: [
        ScheduleFilterPage
    ]
})

export class ScheduleFilterPageModule {}