import { NgModule, ErrorHandler } from '@angular/core';

import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';

import { ConferenceApp } from './app.component';

import { PopoverPage } from '../pages/about-popover/about-popover';
import { AccountPage } from '../pages/account/account';
// import { SignupPage } from '../pages/signup/signup';
import { SupportPage } from '../pages/support/support';
import { DownloadPage} from '../pages/download/download'
import { StarRatingModule } from 'ionic3-star-rating';

import { ConferenceData } from '../providers/conference-data';
import { UserData } from '../providers/user-data';

import { InAppBrowser } from '@ionic-native/in-app-browser';
import { SplashScreen } from '@ionic-native/splash-screen';
import {BrowserModule} from '@angular/platform-browser'
import { HttpModule } from '@angular/http';


import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from "angularfire2/auth";
import { AngularFireDatabaseModule  } from "angularfire2/database";
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { NgxQRCodeModule } from 'ngx-qrcode2'


var firebaseConfig = {
  firebase: {
  apiKey: "AIzaSyAoYcfP5NVElj1FhafipljiS9NHLXc70rM",
  authDomain: "sodveloncon18.firebaseapp.com",
  databaseURL: "https://sodveloncon18.firebaseio.com",
  projectId: "sodveloncon18",
  storageBucket: "sodveloncon18.appspot.com",
  messagingSenderId: "430886862725"
  },
  users_endpoint: "users",
  chats_endpoint: "chats"
};


@NgModule({
  declarations: [
    ConferenceApp,
    AccountPage,
    PopoverPage,
    SupportPage,
    DownloadPage
  ],
  imports: [
    IonicModule.forRoot(ConferenceApp),
    IonicStorageModule.forRoot(),
    AngularFireModule.initializeApp(firebaseConfig.firebase),
    BrowserModule,
    HttpModule,
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    NgxQRCodeModule,
    StarRatingModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    ConferenceApp,
    AccountPage,
    DownloadPage,
    PopoverPage,
    SupportPage
  ],
  providers: [
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    ConferenceData,
    UserData,
    InAppBrowser,
    SplashScreen,
    BarcodeScanner
  ]
})
export class AppModule { }
