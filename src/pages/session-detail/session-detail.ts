import { Component } from '@angular/core';

import { NavParams, IonicPage } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-session-detail',
  templateUrl: 'session-detail.html'
})
export class SessionDetailPage {
  session: any;

  constructor(public navParams: NavParams) {
    this.session = navParams.data;
  }
}
