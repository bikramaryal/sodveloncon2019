import { MapPage } from './map';
import { IonicPageModule } from 'ionic-angular';
import { NgModule } from '@angular/core';


@NgModule({
    declarations: [
        MapPage,
    ],
    imports: [
        IonicPageModule.forChild(MapPage),
    ],
    exports: [
        MapPage
    ]
})

export class MapPageModule {}