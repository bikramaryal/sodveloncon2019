import { SpeakerListPage } from './speaker-list';
import { IonicPageModule } from 'ionic-angular';
import { NgModule } from '@angular/core';


@NgModule({
    declarations: [
        SpeakerListPage,
    ],
    imports: [
        IonicPageModule.forChild(SpeakerListPage),
    ],
    exports: [
        SpeakerListPage
    ]
})

export class SpeakerListPageModule {}