import { SchedulePage } from './schedule';
import { IonicPageModule } from 'ionic-angular';
import { NgModule } from '@angular/core';


@NgModule({
    declarations: [
        SchedulePage,
    ],
    imports: [
        IonicPageModule.forChild(SchedulePage),
    ],
    exports: [
        SchedulePage
    ]
})

export class SchedulePageModule {}